'use strict'

const UserSeeder = require('./UserSeeder')
const TodoSeeder = require('./TodoSeeder')

class DatabaseSeeder {
  async run() {
    await UserSeeder.run()
    await TodoSeeder.run()
  }
}

module.exports = DatabaseSeeder
