'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Database')} */
const Database = use('Database')
const Hash = use('Hash')

class UserSeeder {
  static async run() {
    const encryptedPassword = await Hash.make('123456')

    await Database.table('users').insert([{
        username: 'user1',
        email: 'user1@gmail.com',
        password: encryptedPassword
      },
      {
        username: 'user2',
        email: 'user2@gmail.com',
        password: encryptedPassword
      }
    ])
  }
}

module.exports = UserSeeder
