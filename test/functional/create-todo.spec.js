'use strict'

const Factory = use('Factory')
const {
  test,
  trait
} = use('Test/Suite')('Create Todo')

trait('Test/ApiClient')
trait('Auth/Client')

test('can create a todo if valid data', async ({
  assert,
  client
}) => {
  const user = await Factory.model('App/Models/User').create()

  const data = {
    title: 'Learn Golang',
    description: 'Learn Golang from `Dasar Pemograman Golang` by novalagung'
  }

  const response = await client.post('/api/v1/todo').loginVia(user, 'jwt').send(data).end()

  response.assertStatus(200)
  response.assertJSONSubset({
    message: 'Successfully add todo',
    data: {
      title: data.title,
      description: data.description
    }
  })
})

test('cannot create a todo if not authenticated', async ({
  assert,
  client
}) => {

  const data = {
    title: 'Learn Golang',
    description: 'Learn Golang from `Dasar Pemograman Golang` by novalagung'
  }

  const response = await client.post('/api/v1/todo').send(data).end()

  response.assertStatus(401)
})

test('cannot create a todo if no title', async ({
  assert,
  client
}) => {
  const user = await Factory.model('App/Models/User').create()

  const {
    description
  } = await Factory.model('App/Models/Todo').make()

  const data = {
    description
  }

  const response = await client.post('/api/v1/todo').loginVia(user, 'jwt').send(data).end()

  response.assertStatus(400)
  response.assertJSONSubset([{
    message: 'title is required',
    field: 'title',
    validation: 'required',
  }, ])
})

test('cannot create a todo if title and description are not a string', async ({
  assert,
  client,
}) => {
  const user = await Factory.model('App/Models/User').create()

  const data = {
    title: 123456,
    description: 123456,
  }

  const response = await client.post('/api/v1/todo').loginVia(user, 'jwt').send(data).end()

  response.assertStatus(400)
  response.assertJSONSubset(
    [{
        message: 'title is not a valid string',
        field: 'title',
        validation: 'string'
      },
      {
        message: 'description is not a valid string',
        field: 'description',
        validation: 'string'
      }
    ])
})
