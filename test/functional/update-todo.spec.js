'use strict'

const Factory = use('Factory')

const {
  test,
  trait
} = use('Test/Suite')('Update Todo')

trait('Test/ApiClient')
trait('Auth/Client')

test('can updated todo by id if authenticated', async ({
  client,
  assert
}) => {
  const user = await Factory.model('App/Models/User').create()
  const todo = await Factory.model('App/Models/Todo').create(1)

  const data = {
    title: 'This is my new title',
    description: 'This is my new description'
  }

  const response = await client.patch(`/api/v1/todo/${todo.id}`).loginVia(user, 'jwt').send(data).end()

  response.assertStatus(200)
  response.assertJSONSubset({
    message: 'Successfully updated todo',
    data: {
      title: data.title,
      description: data.description
    }
  })
})

test('cannot updated todo by id if not authenticated', async ({
  client,
  assert
}) => {
  const todo = await Factory.model('App/Models/Todo').create(1)

  const data = {
    title: 'This is my new title',
    description: 'This is my new description'
  }

  const response = await client.patch(`/api/v1/todo/${todo.id}`).send(data).end()

  response.assertStatus(401)
})

test('cannot updated todo by id if id do not exist', async ({
  assert,
  client
}) => {
  const user = await Factory.model('App/Models/User').create()

  const data = {
    title: 'This is my new title',
    description: 'This is my new description'
  }

  const response = await client.patch('/api/v1/todo/999').loginVia(user, 'jwt').send(data).end()

  response.assertStatus(404)
})
