'use strict'

const Factory = use('Factory')

const {
  test,
  trait
} = use('Test/Suite')('Delete Todo')

trait('Test/ApiClient')
trait('Auth/Client')

test('can delete todo by id if authenticated', async ({
  assert,
  client
}) => {
  const user = await Factory.model('App/Models/User').create()
  const todo = await Factory.model('App/Models/Todo').create(1)

  const response = await client.delete(`/api/v1/todo/${todo.id}`).loginVia(user, 'jwt').end()

  response.assertStatus(200)
})

test('cannot delete todo by id if not authenticated', async ({
  assert,
  client
}) => {
  const todo = await Factory.model('App/Models/Todo').create(1)

  const response = await client.delete(`/api/v1/todo/${todo.id}`).end()

  response.assertStatus(401)
})

test('cannot delete todo by id if id do not exist', async ({
  assert,
  client
}) => {
  const user = await Factory.model('App/Models/User').create()

  const response = await client.delete('/api/v1/todo/999').loginVia(user, 'jwt').end()

  response.assertStatus(404)
})
