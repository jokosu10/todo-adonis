'use strict'

const Factory = use('Factory')

const {
  test,
  trait
} = use('Test/Suite')('Get Todo')

trait('Test/ApiClient')
trait('Auth/Client')

test('can get all the todo if authenticated', async ({
  assert,
  client
}) => {
  const user = await Factory.model('App/Models/User').create()
  const todos = await Factory.model('App/Models/Todo').createMany(3)

  const response = await client.get('/api/v1/todo').loginVia(user, 'jwt').end()

  response.assertStatus(200)
  response.assertJSONSubset({
    message: 'Successfully get todos',
    data: [

      {
        id: todos[0].id,
        title: todos[0].title,
        description: todos[0].description
      },
      {
        id: todos[1].id,
        title: todos[1].title,
        description: todos[1].description
      },
      {
        id: todos[2].id,
        title: todos[2].title,
        description: todos[2].description
      }

    ]
  })
})

test('cannot get all the todo if not authenticated', async ({
  assert,
  client
}) => {
  const todos = await Factory.model('App/Models/Todo').createMany(3)

  const response = await client.get('/api/v1/todo').end()

  response.assertStatus(401)
})

test('can get a todo by id if authenticated', async ({
  assert,
  client
}) => {
  const user = await Factory.model('App/Models/User').create()
  const todos = await Factory.model('App/Models/Todo').createMany(3)

  const todo = todos[0]

  const response = await client.get(`/api/v1/todo/${todo.id}`).loginVia(user, 'jwt').end()

  response.assertStatus(200)

  response.assertJSONSubset({
    message: 'Successfully get todo',
    data: {
      id: todo.id,
      title: todo.title,
      description: todo.description
    }
  })
})

test('cannot get todo by id if not authenticated', async ({
  assert,
  client
}) => {
  const todos = await Factory.model('App/Models/Todo').createMany(3)

  const todo = todos[0]

  const response = await client.get(`/api/v1/todo/${todo.id}`).end()

  response.assertStatus(401)
})

test('cannot get todo by id if id do not exist', async ({
  assert,
  client
}) => {
  const user = await Factory.model('App/Models/User').create()

  const response = await client.get('/api/v1/todo/999').loginVia(user, 'jwt').end()
  response.assertStatus(404)
})
