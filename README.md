# Adonis API application

This application for test Software Engineer in Kasir Pintar.

## Prerequisites

Make sure you install this in your workspace.

```bash
1. NodeJS LTS Version (recommended v10.15.x)
2. NPM (recommended v6.9.x)
3. Git (recommended v2.22.x)
4. PostgreSQL (recommended v11.7)
```

## Setup

Step by step for setup this application.

```bash
1. git clone https://gitlab.com/jokosu10/todo-adonis.git
2. cd todo-adonis
3. npm install
4. cp .env.example .env
5. cp .env.testing.example .env.testing
6. set variable in .env
7. set variable in .env.testing
```

## Run server

Run the following command to run server for development mode.

```js
npm run dev
```

### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```

### Seeder

Run the following command to run seed for initial data.

```js
adonis seed --files DatabaseSeeder.js
```

### Testing

Run the following command to run testing application.

```js
adonis test
```

### Run EndPoint Api

Open postman / insomia application. Import collection via link `https://www.getpostman.com/collections/76968cf4b8d02243d631`.

Have fun :)
