'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return {
    greeting: 'Hello world in JSON'
  }
})

Route.group(() => {
  Route.post('login', 'AuthController.postlogin')
  Route.post('refresh', 'AuthController.postRefreshToken').middleware(['auth:jwt'])
  Route.post('logout', 'AuthController.postLogout').middleware(['auth:jwt'])
  Route.get('todo', 'TodoController.index').middleware(['auth:jwt'])
  Route.get('todo/:id', 'TodoController.show').middleware(['auth:jwt'])
  Route.post('todo', 'TodoController.store').validator('Todo').middleware(['auth:jwt'])
  Route.patch('todo/:id', 'TodoController.update').validator('Todo').middleware(['auth:jwt'])
  Route.delete('todo/:id', 'TodoController.delete').middleware(['auth:jwt'])
}).prefix('api/v1/')
