'use strict'

class AuthController {

  async postlogin({
    auth,
    request,
    response
  }) {
    let {
      email,
      password
    } = request.all()

    try {
      if (await auth.attempt(email, password)) {
        return auth
          .authenticator('jwt')
          .withRefreshToken()
          .attempt(email, password)
      }
    } catch (e) {
      console.log(e)
      return response.status(404).json({
        message: 'Your account not registered in database'
      })
    }
  }

  async postRefreshToken({
    request,
    auth
  }) {
    let refreshToken = request.input('refresh_token')
    return await auth
      .newRefreshToken()
      .generateForRefreshToken(refreshToken)
  }

  async postLogout({
    auth,
    response
  }) {
    const apiToken = auth.getAuthHeader()
    await auth
      .authenticator('jwt')
      .revokeTokens([apiToken])

    return response.status(200).json({
      message: 'Logout successfully!'
    })
  }

}

module.exports = AuthController
