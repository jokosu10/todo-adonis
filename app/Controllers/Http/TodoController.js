'use strict'

const Todo = use('App/Models/Todo')

class TodoController {

  async index({
    response
  }) {
    const todos = await Todo.all()
    return response.status(200).json({
      message: 'Successfully get todos',
      data: todos
    })
  }

  async show({
    params,
    response
  }) {
    const todo = await Todo.find(params.id)
    if (!todo) {
      return response.status(404).json({
        message: 'Todo not found'
      })
    } else {
      return response.status(200).json({
        message: 'Successfully get todo',
        data: todo
      })
    }
  }

  async store({
    request,
    response
  }) {
    const todo = new Todo()

    todo.title = request.input('title')
    todo.description = request.input('description')

    await todo.save()
    return response.status(200).json({
      message: 'Successfully add todo',
      data: todo
    })
  }

  async update({
    params,
    request,
    response
  }) {
    const todo = await Todo.find(params.id)
    if (!todo) {
      return response.status(404).json({
        message: 'Todo not found'
      })
    } else {
      todo.title = request.input('title')
      todo.description = request.input('description')

      await todo.save()
      return response.status(200).json({
        message: 'Successfully updated todo',
        data: todo
      })

    }
  }

  async delete({
    params,
    response
  }) {
    const todo = await Todo.find(params.id)
    if (!todo) {
      return response.status(404).json({
        message: 'Todo not found'
      })
    } else {
      await todo.delete()
      return response.status(200).json({
        message: 'Successfully delete todo',
      })
    }
  }

}

module.exports = TodoController
